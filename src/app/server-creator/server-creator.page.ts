import {Component, OnInit} from '@angular/core';
import {OnlineService, RestServerData} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-server-creator',
    templateUrl: './server-creator.page.html',
    styleUrls: ['./server-creator.page.scss'],
})
export class ServerCreatorPage implements OnInit {

    step = 1;
    busy = false;

    data: RestServerData = new RestServerData();

    verificationCode: string;

    constructor(public online: OnlineService, public ux: UXService) {
        this.data.virtualServer = 1;
    }

    step1() {
        if (this.data.host.length < 5) {
            this.ux.showError('You need to enter an server name');
            return;
        }
        this.busy = true;
        this.verificationCode = null;
        this.online.serverCheckOnline(this.data).subscribe(value => {
            console.log('server online? ' + value.toString());
            this.verificationCode = value.name;
            this.busy = false;
            if (value)
                this.step++;
        }, error1 => {
            this.busy = false;
            this.ux.showNetworkError(error1, 'Could not find server');
        });
    }

    step3() {
        this.busy = true;
        this.online.serverCreate(this.data).subscribe(value => {
            this.busy = false;
            console.log('server created: ' + value);
        }, error => {
            this.busy = false;
            this.ux.showNetworkError(error, 'Could not create server');
        });
        /*this.online.serverCheckVname(this.data).subscribe(value => {
            console.log('query credentials are ' + value);
            this.step++;
        }, error1 => {
            this.busy = false;
            this.ux.showNetworkError('Server is not reachable', error1);
        });*/
    }

    ngOnInit() {
    }

}
