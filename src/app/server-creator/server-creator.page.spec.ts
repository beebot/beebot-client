import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerCreatorPage } from './server-creator.page';

describe('ServerCreatorPage', () => {
  let component: ServerCreatorPage;
  let fixture: ComponentFixture<ServerCreatorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerCreatorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerCreatorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
