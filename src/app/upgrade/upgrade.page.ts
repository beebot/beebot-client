import {Component, OnInit} from '@angular/core';
import {OnlineService, SessionData} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-upgrade',
    templateUrl: './upgrade.page.html',
    styleUrls: ['./upgrade.page.scss'],
})
export class UpgradePage implements OnInit {

    session: SessionData;

    privs: [
        'Standard User Account',
        'Premium Account',
        'Star*',
        'Administrator Account'
    ];

    constructor(public online: OnlineService, public ux: UXService) {

    }

    ngOnInit() {
        this.online.userStatus().subscribe(value => {
            this.session = value;
        }, error1 => this.ux.showNetworkError(error1));
    }

}
