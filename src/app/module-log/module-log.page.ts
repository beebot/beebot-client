import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataState, ModuleStatusMessage, OnlineService} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-module-log',
    templateUrl: './module-log.page.html',
    styleUrls: ['./module-log.page.scss'],
})
export class ModuleLogPage implements OnInit {


    sid: string;
    id: string;

    logs: Array<ModuleStatusMessage> = [];
    state: DataState = DataState.LOADING;

    constructor(public route: ActivatedRoute, public online: OnlineService, public ux: UXService) {
    }

    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');
        this.id = this.route.snapshot.paramMap.get('id');

        this.update();
    }

    update() {
        this.state = DataState.LOADING;
        this.online.moduleLog(this.sid, this.id).subscribe(value => {
            this.logs = value;
            this.state = DataState.OKAY;
        }, error => {
            this.state = DataState.WRONG;
            this.ux.showError(error);
        });
    }

}
