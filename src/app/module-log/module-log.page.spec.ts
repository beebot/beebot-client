import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleLogPage } from './module-log.page';

describe('ModuleLogPage', () => {
  let component: ModuleLogPage;
  let fixture: ComponentFixture<ModuleLogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleLogPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleLogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
