import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: './home/home.module#HomePageModule'
    },
    {path: 'login', loadChildren: './login/login.module#LoginPageModule'},
    {path: 'upgrade', loadChildren: './upgrade/upgrade.module#UpgradePageModule'},
    {path: 'server', loadChildren: './server/server.module#ServerPageModule'},
    {path: 'server/create', loadChildren: './server-creator/server-creator.module#ServerCreatorPageModule'},
    {path: 'modules/:sid/create', loadChildren: './module-creation/module-creation.module#ModuleCreationPageModule'},
    {path: 'modules/:sid/edit/:id', loadChildren: './module-edit/module-edit.module#ModuleEditPageModule'},
    {path: 'monitor/:sid', loadChildren: './monitor/monitor.module#MonitorPageModule'},
    {path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule'},
    {path: 'log', loadChildren: './log/log.module#LogPageModule'},
    {path: 'poke/:sid', loadChildren: './poke/poke.module#PokePageModule'},
    {path: 'contact', loadChildren: './contact/contact.module#ContactPageModule'},
    {path: 'dashboard/:sid', loadChildren: './dashboard/dashboard.module#DashboardPageModule'},
    {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule'},
    {path: 'gdpr/:key', loadChildren: './gdpr/gdpr.module#GdprPageModule'},
    {
        path: 'gdpr',
        redirectTo: 'gdpr/show',
        pathMatch: 'full'
    },
  { path: 'modules/:sid/log/:id', loadChildren: './module-log/module-log.module#ModuleLogPageModule' }


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
