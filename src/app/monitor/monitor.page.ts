import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {BasicServerStat, OnlineService, ServerStatBean, ServerTreeItem} from '../online.service';

import {Chart} from 'chart.js';
import {ActivatedRoute, Router} from '@angular/router';
import {UXService} from '../ux.service';
import {ModalController, PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-monitor',
    templateUrl: './monitor.page.html',
    styleUrls: ['./monitor.page.scss'],
})
export class MonitorPage implements OnInit {


    @ViewChildren('monitors') monitors: QueryList<ElementRef>;


    sid: string;
    stats: BasicServerStat;

    pokeModal = false;

    tree: ServerTreeItem;

    mins = 3 * 60;

    constructor(public online: OnlineService, public router: Router, public ux: UXService, private route: ActivatedRoute,
                private modal: ModalController, private popover: PopoverController) {
        this.stats = new BasicServerStat();
        this.stats.stats = new ServerStatBean();
    }

    update() {
        this.online.serverStats(this.sid).subscribe(value => {
            this.stats = value;
        }, error1 => this.ux.showNetworkError(error1));

        this.online.teamspeakTree(this.sid).subscribe(value => {
            this.tree = value;
        }, error1 => this.ux.showNetworkError(error1));
    }

    showPokeModal() {
        /*const m = this.modal.create({
            component: UserPokeComponent,
            componentProps: {'sid': this.sid}
        });
        m.then(value => value.present());*/
        /*const p = this.popover.create({
            component: UserPokeComponent,
            componentProps: {'sid': this.sid}
        });
        p.then(v => v.present());*/
        this.pokeModal = true;
    }


    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');
        this.update();
    }

}
