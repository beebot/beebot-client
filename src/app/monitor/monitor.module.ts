import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {MonitorPage} from './monitor.page';
import {ModuleMonitorModule} from '../module-monitor/module-monitor.module';
import {ModuleComponentsModule} from '../module-components/module-components.module';

const routes: Routes = [
    {
        path: '',
        component: MonitorPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ModuleMonitorModule,
        RouterModule.forChild(routes),
        ModuleComponentsModule
    ],
    declarations: [MonitorPage]
})
export class MonitorPageModule {
}
