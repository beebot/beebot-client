import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BasicModuleReference, DataState, OnlineService, Updatable} from '../../online.service';
import {UXService} from '../../ux.service';

@Component({
    selector: 'app-monitor-module-card',
    templateUrl: './monitor-module-card.component.html',
    styleUrls: ['./monitor-module-card.component.scss']
})
export class MonitorModuleCardComponent implements OnInit, Updatable, OnChanges {

    modules: Array<BasicModuleReference>;

    title = "█ Modules";
    text = "Loading...";

    state = DataState.LOADING;

    @Input() sid: string;

    constructor(public online: OnlineService, public ux: UXService) {
        this.calc();
    }

    ngOnInit() {
        this.calc();
    }

    calc() {
        this.title = 'Modules';
        this.text = 'Loading...';
        if (this.sid == null) {
            console.log('SID is null');
            return;
        }
        this.online.moduleActive(this.sid).subscribe(value => {
            this.modules = value;
            let okayVar = 0;
            this.modules.forEach(mod => {
                if (mod.status.okay) {
                    okayVar++;
                }
            });
            this.state = (okayVar === this.modules.length) ? DataState.OKAY : DataState.WRONG;
            this.title = 'Modules';
            this.text = this.modules.length + ' modules active\n';
            if (this.state == DataState.OKAY) {
                this.text += 'No errors';
            } else {
                this.text += (this.modules.length - okayVar) + ' not working';
            }
        }, error1 => {
            this.state = DataState.WRONG;
            this.title = 'Modules';
            this.ux.showNetworkError(error1);
        });

    }

    ngOnChanges(changes: SimpleChanges): void {
        this.calc();
    }

}
