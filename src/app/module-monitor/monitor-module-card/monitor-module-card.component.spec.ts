import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorModuleCardComponent } from './monitor-module-card.component';

describe('MonitorModuleCardComponent', () => {
  let component: MonitorModuleCardComponent;
  let fixture: ComponentFixture<MonitorModuleCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorModuleCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorModuleCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
