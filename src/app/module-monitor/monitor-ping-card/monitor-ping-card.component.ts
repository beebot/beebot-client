import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DataState, Updatable} from '../../online.service';

@Component({
    selector: 'app-monitor-ping-card',
    templateUrl: './monitor-ping-card.component.html',
    styleUrls: ['./monitor-ping-card.component.scss']
})
export class MonitorPingCardComponent implements OnInit, Updatable, OnChanges {

    @Input() ping: number;

    state = DataState.LOADING;
    value: string;

    constructor() {
        this.calc();
    }

    ngOnInit() {
        this.calc();
    }

    calc() {
        let value;
        this.value = 'Calculating';
        if (this.ping == null) {
            this.state = DataState.LOADING;
            return;
        }

        this.state = (this.ping < 250) ? DataState.OKAY : DataState.WRONG;

        if (this.ping < 30) {
            value = 'Perfect Ping';
        } else if (this.ping < 50) {
            value = 'Good Ping';
        } else if (this.ping < 100) {
            value = 'Slightly laggy';
        } else if (this.ping < 250) {
            value = 'Almost bad';
        } else if (this.ping < 750) {
            value = 'Bad Ping';
        } else {
            value = 'Terrible Ping';
        }

        this.value = Math.round(this.ping) + 'ms\n' + value;

    }

    ngOnChanges(changes: SimpleChanges): void {
        this.calc();
    }
}
