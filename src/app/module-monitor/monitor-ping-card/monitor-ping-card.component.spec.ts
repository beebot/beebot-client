import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorPingCardComponent } from './monitor-ping-card.component';

describe('MonitorPingCardComponent', () => {
  let component: MonitorPingCardComponent;
  let fixture: ComponentFixture<MonitorPingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorPingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorPingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
