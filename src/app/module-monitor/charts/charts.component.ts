import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Chart} from 'chart.js';
import {OnlineService, StatType} from '../../online.service';
import {UXService} from '../../ux.service';
import {PluginsService} from '../../plugins.service';

@Component({
    selector: 'app-charts',
    templateUrl: './charts.component.html',
    styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit, AfterViewInit, OnChanges {

    COLORS = [
        '#003f5c88',
        '#12456a88',
        '#27497788',
        '#3e4d8388',
        '#56508c88',
        '#6e519288',
        '#87529588',
        '#a0519588',

        '#bf508f',
        '#da5184',
        '#f05774',
        '#ff6460',
        '#ff7749',
        '#ff8d2f',
        '#ffa600',

    ];

    @ViewChild('lineCanvas') private chartRef;
    chart;

    @Input() name = 'untitled chart';
    @Input() sid: string;
    @Input() percentage = false;
    @Input() percentageSlider = true;
    @Input() type: StatType[];
    @Input() stacked = true;
    @Input() mins = 3 * 60;
    @Input() noFill = false;


    @Input() avgMaxMin = false;
    @Input() avgMaxMinColorRevert = false;

    @Input() uiSize;

    selectedType: StatType;

    StatType = StatType;

    data;

    constructor(public online: OnlineService, public ux: UXService, public plugin: PluginsService) {
    }

    ngOnInit() {
        if (this.selectedType == null && this.type.length > 0) {
            this.selectedType = this.type[0];
        }

        const _this = this;

        this.chart = new Chart(this.chartRef.nativeElement, {

            type: 'line',
            data: {
                labels: ['Loading...'],
                datasets: [{
                    label: '# of Votes',
                    data: [0, 8, 1, 5],
                    borderWidth: 1,
                }]
            },
            options: {
                spanGaps: true,
                tooltips: {
                    mode: 'label', // show all datasets
                    callbacks: {
                        label: function (item, data) {
                            let label = data.datasets[item.datasetIndex].label || '???';
                            label += ' : ';

                            if (item.yLabel === 0) {
                                return;
                            }

                            if (_this.percentage) {
                                return label + Math.round(+item.yLabel * 100) + '%';
                            }
                            return label + '' + Math.round(+item.yLabel * 10) / 10 + '';
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                if (_this.percentage) {
                                    return Math.round(value * 100) + '%';
                                }
                                return value;
                            }
                        }
                    }]
                }
            }

        });
        this.update();

    }


    ngOnChanges(changes: SimpleChanges): void {
        this.update();
    }

    update() {
        if (this.sid == null || this.selectedType == null) {
            return;
        }

        this.online.stats(this.sid, this.selectedType, this.mins).subscribe(value => {
            this.data = value;
            this.calc();
        }, error1 => {
            this.ux.showNetworkError(error1);
        });
    }

    getDateSimple(key) {
        const newDate: Date = new Date(Date.parse(key));

        const dayMonth = newDate.getDate() + '.' + (newDate.getMonth() + 1);
        const hourMins = this.doubleDigit(newDate.getHours()) + ':' + this.doubleDigit(newDate.getMinutes());
        if (this.mins <= 24 * 60) {
            return hourMins;
        }
        if (this.mins <= 7 * 24 * 60) {
            return dayMonth + ' ' + hourMins;
        }
        return dayMonth;
    }

    calc() {
        this.prepareCalculation();

        // Set Datasets
        this.getLabels().forEach(label => {
            const values = this.getValuesForLabel(label);

            this.chart.data.datasets.push({
                label: label,
                data: values
            });
        });

        this.calcMissingDataPoints();
        this.calcColors();
        this.calcScales();
        this.calcAvgType();


        // Finish
        this.chart.update();
    }

    /**
     * Hashes a string to an integer. Not secure, just for color display!
     * @param s the string. not null
     */
    hash(s: string): number {
        const s2 = s.toUpperCase();
        let n = 0;
        for (let i = 0; i < s2.length; ++i) {
            n += s2.charCodeAt(i) * 2437;
        }
        return n;
    }

    /**
     * converts a hsl color to an #hex color
     * @param h hue
     * @param s saturation
     * @param l lightness
     */
    hslToHex(h, s, l) {
        h /= 360;
        s /= 100;
        l /= 100;
        let r, g, b;
        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            const hue2rgb = (p, q, t) => {
                if (t < 0) {
                    t += 1;
                }
                if (t > 1) {
                    t -= 1;
                }
                if (t < 1 / 6) {
                    return p + (q - p) * 6 * t;
                }
                if (t < 1 / 2) {
                    return q;
                }
                if (t < 2 / 3) {
                    return p + (q - p) * (2 / 3 - t) * 6;
                }
                return p;
            };
            const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            const p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }
        const toHex = x => {
            const hex = Math.round(x * 255).toString(16);
            return hex.length === 1 ? '0' + hex : hex;
        };
        return 'rgba(' + r * 255 + ',' + g * 255 + ',' + b * 255 + ',.5)';
    }

    /**
     * turns an number to an string with a leading zero if needed
     * @param n
     */
    doubleDigit(n: number): string {
        return (n >= 0 && n < 10) ? '0' + n : '' + n;
    }

    /**
     * @return all datapoint times
     */
    getTimes(): Array<string> {
        return Object.keys(this.data);
    }

    private prepareCalculation() {
        // prepare dates
        const newData = {};
        Object.keys(this.data).forEach(key => {
            const dateString = this.getDateSimple(key);
            newData[dateString] = this.data[key];
        });

        // remove edge dates
        delete newData[Object.keys(newData)[0]];
        delete newData[Object.keys(newData)[Object.keys(newData).length - 1]];

        // set the new, filtered date
        this.data = newData;


        this.chart.data.labels = this.getTimes();

        this.chart.data.datasets = [];
    }

    private calcMissingDataPoints() {
        // Remove missing datapoints (=0)
        this.chart.data.datasets.forEach(dataset => {
            dataset.data.forEach((data, index, array) => {
                array[index] = data !== 0 ? data : null;
            });
        });
    }

    private calcColors() {
        // Set Colors
        this.chart.data.datasets.forEach(dataset => {
            const label = dataset.label;
            const hash = this.hash(label);

            // const hue = hash / 8;
            // const value = hash % 8;
            // const color = this.hslToHex(hue / 32 * 360, 80, 0.5 + (value / 7 / 2 * 100));
            const color = this.COLORS[hash % this.COLORS.length];

            dataset.fillColor = color;
            dataset.backgroundColor = color;

            // check NOFILL option
            dataset.fill = !this.noFill;
        });

        let max = 0;

        this.chart.data.datasets.forEach(dataset => {
            dataset.data.forEach(v => {
                if (v > max) {
                    max = v;
                }
            });
        });
    }

    private calcScales() {
        // Set Scales
        if (this.percentage) {
            this.chart.options.scales.yAxes[0].ticks.max = 1;
        } else {
            delete this.chart.options.scales.yAxes[0].ticks.max;
        }

        this.chart.options.scales.yAxes[0].stacked = this.stacked;
    }

    private calcAvgType() {
        if (this.avgMaxMin) {
            const avgId = 0;
            const maxId = 1;
            const minId = 2;

            const red = 'rgba(255,0,0,.2)';
            const green = 'rgba(0,255,0,.2)';

            console.log(this.avgMaxMinColorRevert + 'revert ' + this.name);


            // create a colored fill between avg-min and avg-max
            this.chart.data.datasets[maxId].fillBetweenSet = avgId;
            this.chart.data.datasets[maxId].fillBetweenColor = this.avgMaxMinColorRevert ? green : red;
            this.chart.data.datasets[minId].fillBetweenSet = avgId;
            this.chart.data.datasets[minId].fillBetweenColor = this.avgMaxMinColorRevert ? red : green;
            // remove point from min and max for a cleaner image
            this.chart.data.datasets[minId].pointRadius = 0;
            this.chart.data.datasets[maxId].pointRadius = 0;

        }
    }

    getValuesForLabel(label: string) {
        const values = [];
        Object.keys(this.data).forEach(key => {
            const d: Array<number> = this.data[key];
            if (d[label] == null) {
                values.push(0);
                return;
            }

            let allValues = 0;
            const value = d[label];
            if (!this.percentage) {
                values.push(value);
                return;
            }
            Object.values(d).forEach(v2 => allValues += v2);

            values.push((allValues !== 0) ? (value / allValues) : 0);

        });
        return values;
    }

    getLabels(): Array<string> {
        const labels = [];
        Object.keys(this.data).forEach(key => {
            Object.keys(this.data[key]).forEach(key2 => {
                labels.push(key2);
            });
        });
        return labels.filter((value, index, array) => {
            return array.indexOf(value) === index;
        }).sort();
    }

    ngAfterViewInit() {

    }

    setFullView(full: boolean) {
        this.uiSize.xl = this.uiSize.xl == 6 ? 12 : 6;
        console.log('set to' + this.uiSize.xl);
    }


}
