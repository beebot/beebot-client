import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DataState, Updatable} from '../../online.service';

@Component({
    selector: 'app-monitor-user-card',
    templateUrl: './monitor-user-card.component.html',
    styleUrls: ['./monitor-user-card.component.scss']
})
export class MonitorUserCardComponent implements OnInit, Updatable, OnChanges {

    @Input() currentUser: number;
    @Input() maxUser: number;

    state: DataState;
    value: string;

    constructor() {
        this.calc();
    }

    ngOnInit() {
        this.calc();
    }

    calc() {
        this.state = DataState.LOADING;
        this.value = 'Calculating...';
        if (this.currentUser == null || this.maxUser == null) {
            return;
        }
        let userS;
        if (this.currentUser === 0) {
            userS = 'Nobody online';
            this.state = DataState.WRONG;
        } else if (this.currentUser * 1.2 > this.maxUser) {
            this.state = DataState.WRONG;
            userS = 'Server is full!';
        } else {
            this.state = DataState.OKAY;
            userS = '';
        }
        this.value = this.currentUser + ' User online \n' + this.maxUser + ' User maximum';
    }


    ngOnChanges(changes: SimpleChanges): void {
        this.calc();
    }

}
