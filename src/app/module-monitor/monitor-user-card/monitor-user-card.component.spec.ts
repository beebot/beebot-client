import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorUserCardComponent } from './monitor-user-card.component';

describe('MonitorUserCardComponent', () => {
  let component: MonitorUserCardComponent;
  let fixture: ComponentFixture<MonitorUserCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorUserCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
