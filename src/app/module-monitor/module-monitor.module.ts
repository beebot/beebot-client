import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MonitorCardComponent} from './monitor-card/monitor-card.component';
import {MonitorPingCardComponent} from './monitor-ping-card/monitor-ping-card.component';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {MonitorUserCardComponent} from './monitor-user-card/monitor-user-card.component';
import {MonitorSoftwareCardComponent} from './monitor-software-card/monitor-software-card.component';
import {MonitorModuleCardComponent} from './monitor-module-card/monitor-module-card.component';
import {ChartsComponent} from './charts/charts.component';
import {ServertreeComponent} from './servertree/servertree.component';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule
    ],
    declarations: [MonitorCardComponent, MonitorPingCardComponent, MonitorUserCardComponent, MonitorSoftwareCardComponent, MonitorModuleCardComponent, ChartsComponent, ServertreeComponent],
    exports: [MonitorCardComponent, MonitorPingCardComponent, MonitorUserCardComponent, MonitorSoftwareCardComponent, MonitorModuleCardComponent, ChartsComponent, ServertreeComponent]
})
export class ModuleMonitorModule {
}
