import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorSoftwareCardComponent } from './monitor-software-card.component';

describe('MonitorSoftwareCardComponent', () => {
  let component: MonitorSoftwareCardComponent;
  let fixture: ComponentFixture<MonitorSoftwareCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorSoftwareCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorSoftwareCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
