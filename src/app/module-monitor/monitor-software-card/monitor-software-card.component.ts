import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DataState, OnlineService, SoftwareType, Updatable, Version} from '../../online.service';
import {UXService} from '../../ux.service';

@Component({
    selector: 'app-monitor-software-card',
    templateUrl: './monitor-software-card.component.html',
    styleUrls: ['./monitor-software-card.component.scss']
})
export class MonitorSoftwareCardComponent implements OnInit, Updatable, OnChanges {

    @Input() type: SoftwareType;

    value: string;
    state = DataState.LOADING;

    currentVersion: Version = new Version();
    updateVersion: Version = new Version();

    constructor(public online: OnlineService, public ux: UXService) {
        this.calc();
    }

    ngOnInit() {
        this.calc();

    }

    calc() {
        this.value = 'Calculating...';
        if (!this.type || this.type == null) {
            this.state = DataState.LOADING;
            return;
        }
        this.online.miscLatestVersion(this.type.platform).subscribe(value1 => {
            const version = new Version();
            version.major = value1.major;
            version.minor = value1.minor;
            version.fix = value1.fix;
            this.updateVersion = version;
            this.calc2();
        }, error1 => {
            this.state = DataState.WRONG;
            this.value = '[Network error]';
            this.ux.showNetworkError(error1);
        });

    }

    calc2() {

        const version = this.type.version;
        if (version.indexOf('[') > 0) {
            this.currentVersion.ofVersion(version.substr(0, version.indexOf('[')));
        }
        this.state = this.updateVersion.isNewerThan(this.currentVersion) ? DataState.WRONG : DataState.OKAY;

        this.value = 'Running: Teamspeak ' + this.currentVersion.toString() +
            ' on ' + this.type.platform + '\nCurrent: Linux ' + this.updateVersion.toString();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.calc();
    }
}
