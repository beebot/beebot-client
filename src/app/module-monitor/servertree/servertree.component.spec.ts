import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServertreeComponent } from './servertree.component';

describe('ServertreeComponent', () => {
  let component: ServertreeComponent;
  let fixture: ComponentFixture<ServertreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServertreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServertreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
