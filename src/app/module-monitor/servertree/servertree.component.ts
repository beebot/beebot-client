import {Component, Input, OnInit} from '@angular/core';
import {DataState, OnlineService, ServerTreeItem} from '../../online.service';
import {UXService} from '../../ux.service';

@Component({
    selector: 'app-servertree',
    templateUrl: './servertree.component.html',
    styleUrls: ['./servertree.component.scss']
})
export class ServertreeComponent implements OnInit {

    @Input() sid: string;

    @Input() tree: ServerTreeItem;
    treeState: DataState = DataState.LOADING;

    outputTree: ServerTreeItem;

    @Input() level = 0;

    constructor(public online: OnlineService, public ux: UXService) {
    }

    ngOnInit() {
        this.reload();
    }

    reload() {
        if (this.sid != null) {
            this.online.teamspeakTree(this.sid).subscribe(value => {
                this.treeState = DataState.OKAY;
                this.tree = value;
                this.outputTree = this.tree;
            }, error => {
                this.treeState = DataState.WRONG;
                this.ux.showNetworkError(error);
            });
        } else {
            //to avoid recusrive lag in angular
            this.treeState = DataState.OKAY;
            this.outputTree = new ServerTreeItem();
            this.outputTree.name = this.tree.name;
            this.outputTree.clients = this.tree.clients;
            setTimeout(() => {
                this.outputTree.subchannel = this.tree.subchannel;
            }, 10);
        }
    }

}
