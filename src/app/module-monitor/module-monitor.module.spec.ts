import { ModuleMonitorModule } from './module-monitor.module';

describe('ModuleMonitorModule', () => {
  let moduleMonitorModule: ModuleMonitorModule;

  beforeEach(() => {
    moduleMonitorModule = new ModuleMonitorModule();
  });

  it('should create an instance', () => {
    expect(moduleMonitorModule).toBeTruthy();
  });
});
