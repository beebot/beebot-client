import {Component, Input, OnInit} from '@angular/core';
import {DataState, Updatable} from '../../online.service';

@Component({
    selector: 'app-monitor-card',
    templateUrl: './monitor-card.component.html',
    styleUrls: ['./monitor-card.component.scss']
})
export class MonitorCardComponent implements OnInit, Updatable {

    @Input() title = '██████';
    @Input() value = '███████████████';
    @Input() state: DataState = DataState.LOADING;

    constructor() {
    }

    ngOnInit() {
    }

    calc() {
        console.log('Wrong function call');
    }

}
