import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BasicServerStat, OnlineService, ServerStatBean, ServerTreeItem} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    sid: string;
    stat: BasicServerStat;

    tab = 'overview';

    serverTree: ServerTreeItem;
    graphMinutes = 24 * 60;


    constructor(public route: ActivatedRoute, public online: OnlineService, public ux: UXService) {
        this.stat = new BasicServerStat();
        this.stat.stats = new ServerStatBean();
    }

    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');

        this.update();
    }

    update() {
        this.online.serverStats(this.sid).subscribe(value => {
            this.stat = value;
        }, error => this.ux.showNetworkError(error));
        this.online.teamspeakTree(this.sid).subscribe(value => {
            this.serverTree = value;
        }, error => this.ux.showNetworkError(error));
    }

    tabChange(event) {
        console.log('changed tab to ' + this.tab);
    }

    reload(event) {
        this.update();
        setTimeout(() => event.target.complete(), 2000);
    }

}
