import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {UXService} from './ux.service';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class OnlineService {

    BASE_URL = 'https://services.karlthebee.de:8080/';

    token: string;

    serverState: DataState = DataState.LOADING;
    server: Array<BasicServerStat> = [];

    selectedServer: BasicServerStat;


    constructor(public storage: Storage, public http: HttpClient, public ux: UXService, public router: Router) {
        // @ts-ignore
        const promise = storage.get('application-token');
        promise.then(value => {
            this.token = value;
            if (this.token == null) {
                return;
            }
            console.log('loaded token ' + value);
            this.serverUpdate();
        });
    }

    setToken(token: string) {
        this.storage.set('application-token', token);
        this.token = token;
    }

    private post<T>(url: string, data: any = '', loggedIn = true): Observable<T> {
        if (loggedIn && !this.token) {
            console.log('No login for ' + url);
            throw throwError('You need to be logged in');
        }
        console.log('>>> ' + this.BASE_URL + url + ' -> token:=' + loggedIn + ' -> data:=' + data);
        if (!loggedIn) {
            return this.http.post<T>(this.BASE_URL + url, data);
        }
        return this.http.post<T>(this.BASE_URL + url + '?token=' + this.token, data);
    }

    /**
     *
     * USER
     */
    public login(mail: string, password: string) {
        const x: LoginCredentials = {
            'mail': mail,
            'password': password
        };
        return this.http.post<LoginReturn>(this.BASE_URL + 'user/login', x);
    }

    public userStatus() {
        return this.post<SessionData>('user/status');
    }

    public userPasswordChange(data: PasswordChangeData) {
        return this.post('user/changepassword', data);
    }

    /**
     *
     * SERVER
     */
    public serverCheckOnline(data: RestServerData): Observable<ServerValidationReturn> {
        return this.post<ServerValidationReturn>('server/checkOnline', data);
    }

    public servers(force: boolean = false): Observable<Array<BasicServerStat>> {
        if (!force && this.server != null && this.server.length != 0) {
            return of(this.server);
        }
        console.log('Starting update');
        this.serverState = DataState.LOADING;
        return this.post<Array<BasicServerStat>>('server').pipe(map(value => {
            this.server = value;
            this.serverState = DataState.OKAY;
            if (value.length > 0 && this.selectedServer == null) {
                this.selectedServer = value[0];
                console.log('Auto selected server "' + this.selectedServer.name + '"');
            }
            return value;
        }));
    }

    public serverUpdate() {
        this.servers().subscribe(value => console.log('update complete'), error1 => {
            this.ux.showNetworkError(error1);
        });
    }

    public serverStats(sid: string) {
        return this.post<BasicServerStat>('server/' + sid + '/stats');
    }


    public serverCheckVname(data: RestServerData) {
        return this.post('server/checkVname', data);
    }

    public serverCreate(data: RestServerData) {
        return this.post('server/create', data);
    }

    public sessionData() {
        return this.http.get(this.BASE_URL + 'user/status?token=' + this.token);
    }

    /**
     * MODULES
     */
    public moduleAll() {
        return this.post<Array<ModuleInfo>>('modules/all');
    }

    public moduleActive(sid: string) {
        return this.post<Array<BasicModuleReference>>('modules/' + sid + '/active');
    }

    public moduleCreate(sid: BasicServerStat, module: ModuleInfo, data: ModuleDataObject) {
        return this.post('modules/' + sid.id + '/' + module.uniqueName + '/create', data);
    }

    public moduleDelete(sid: string, moduleId: number) {
        return this.post('modules/' + sid + '/' + moduleId + '/delete');
    }

    public moduleInfo(sid: string, moduleId: number) {
        return this.post<BasicModuleReference>('modules/' + sid + '/' + moduleId + '/info');
    }

    public moduleLog(sid: string, moduleId) {
        return this.post<Array<ModuleStatusMessage>>('modules/' + sid + '/' + moduleId + '/log');
    }

    public moduleData(sid: string, moduleId: number) {
        return this.post<ModuleDataObject>('modules/' + sid + '/' + moduleId + '/data');
    }

    public moduleChange(sid: string, moduleId: number, data: ModuleDataObject) {
        return this.post('modules/' + sid + '/' + moduleId + '/change', data);
    }

    /**
     * TEAMSPEAK
     */
    public teamspeakChannelGroups(sid: string) {
        return this.post<Array<Tuple>>('teamspeak/' + sid + '/channelGroups');
    }

    public teamspeakServerGroups(sid: string) {
        return this.post<Array<Tuple>>('teamspeak/' + sid + '/serverGroups');
    }

    public teamspeakChannel(sid: string) {
        return this.post<Array<Tuple>>('teamspeak/' + sid + '/channel');
    }

    public teamspeakUser(sid: string) {
        return this.post<Array<Tuple>>('teamspeak/' + sid + '/user');
    }

    public teamspeakPoke(sid: string, poke: PokeBody) {
        return this.post<Array<String>>('teamspeak/' + sid + '/poke', poke);
    }

    public teamspeakTree(sid: string) {
        return this.post<ServerTreeItem>('teamspeak/' + sid + '/tree');
    }

    public teamspeakClients(sid: string) {
        return this.post<Array<DetailedClientView>>('teamspeak/' + sid + '/clients');
    }

    /**
     * STATS
     */

    public stats(sid: string, type: StatType, mins: number) {
        const typeName = StatType[type];
        return this.post('stats/' + sid + '/' + typeName + '/' + mins);
    }

    /**
     * MISC
     */
    public miscBlockedWords() {
        return this.post<Array<BlockedWordList>>('misc/blockedWords');
    }

    public miscLatestVersion(version: string) {
        return this.post<Version>('misc/latestVersion/' + version);
    }

    /**
     * GDPR
     */

    public gdprGet(key) {
        return this.post<any>('gdpr/get/key/' + key);
    }

    /**
     * LOGS
     */
    public adminLogs() {
        return this.post<Array<string>>('admin/logs');
    }

    public adminLog(log: string) {
        return this.post<Array<string>>('admin/logs/' + log);
    }

    public adminUser() {
        return this.post('admin/user');
    }

    public logout() {
        this.storage.set('application-token', null);
        this.token = null;
        this.router.navigateByUrl('/');
    }
}

export class ModuleInfo {
    uniqueName: string;
    name: string;
    description: string;
    forced: false;
    author: string;
    convention: any;
}

export class BasicServerStat {
    host: string;
    id: string;
    stats: ServerStatBean;
    name: string;
}

export class ServerStatBean {
    created: Date;
    online: boolean;
    ping: number;
    userCurrent: number;
    userMax: number;
    type: SoftwareType;
    name: string;
}

export class SoftwareType {
    version: string;
    platform: string;
}

export class ServerTreeItem {
    name: string;
    subchannel: Array<ServerTreeItem>;
    clients: Array<string>;
}

export class RestServerData {
    host = '';
    queryName = '';
    queryPassword = '';
    virtualServer = 1;
    disabled: boolean;
    flood: boolean;
}

export class LoginCredentials {
    mail: string;
    password: string;
}


export interface Updatable {
    calc();
}

export enum DataState {
    LOADING,
    OKAY,
    WRONG
}

export class SessionData {
    name: string;
    mail: string;
    state: UserBeanState;
    privilege: Privileges;
}

export enum Privileges {
    USER,
    PREMIUM,
    STAR,
    ADMIN
}

export class LoginReturn {
    token: string;
    validUntil: Date;
    name: string;
}

export class ServerValidationReturn {
    valid: boolean;
    name: string;
}

export enum UserBeanState {
    UNCONFIRMED,
    BANNED,
    USER,
    PREMIUM,
    STAR,
    ADMIN
}

export class StatRequest {
    from;
    to;
}

export enum StatType {
    PLATFORM,
    COUNTRY,
    REGION,
    CITY,
    ISP,
    VERSION,
    PING,
    USER
}

// tslint:disable-next-line:no-empty-interface
export interface ModuleDataObject {
}

export class PrivateChannelsData implements ModuleDataObject {
    channel: ChannelReference = new ChannelReference();
    message: string;
    adminGroup: ChannelGroupReference = new ChannelGroupReference();

    channelName: string;
    channelDescription: string;
    deleteDelay: number;
}

export class CtafkData implements ModuleDataObject {
    showAll: boolean;
    showLatest: boolean;
    winnerLatest: boolean;

    line: string;
    channelName: string;
    channelDescription: string;

    maxUser: number;
    updateTime: number;
    channel: ChannelReference = new ChannelReference();
}

export class ChannelReference {
    channelId: number;
}

export class ChannelGroupReference {
    groupId: number;
}

export class ServerGroupReference {
    groupId: number;
}

export class Tuple {
    key: any;
    value: any;
}

export class BasicModuleReference {
    id: number;
    uniqueName: string;
    name: string;

    status: ModuleStatusMessage;

    stateName: string;
}

export class ModuleStatusMessage {
    date: string;
    message: string;
    okay: boolean;
}

export class PasswordChangeData {
    oldPassword: string;
    newPassword: string;
}

export class DetailedClientView {
    uid: string;
    nickname: string;
    joined: string;
    ip: string;
    zip: string;
    city: string;
    country: string;
    longitude: number;
    latitude: number;
}

export class BlockedWordList {
    name: string;
    blockedWords: Array<string>;
}

export class AntiSpamData implements ModuleDataObject {
    blockDefaultUsername: boolean;
    blockCensoredUsernames: boolean;
    blockChannelHopping: boolean;

    hoppingTimes: number;
    hoppingSeconds: number;

    strategy;

    activatedLists: Array<string>;
    ownBlockwords: Array<string>;
    ownWhitelist: Array<string>;

}

export class SupportData implements ModuleDataObject {
    channel: ChannelReference;
    group: ServerGroupReference;
    userMessage: string;
    supportMessage: string;
    description: string;
    showOnline: boolean;
}

export class MessageData implements ModuleDataObject {
    group: ServerGroupReference;
    channel: ChannelReference;
    start: string;
    end: string;
    message: string;
}

export class PokeBody {
    message: string;
    ids: Array<number>;
}

export class Version {
    major = -1;
    minor = -1;
    fix = -1;
    valid = true;

    public ofVersion(version: string) {
        const split = version.split('.');
        this.major = parseInt(split[0], 10);
        this.minor = parseInt(split[1], 10);
        this.fix = parseInt(split[2], 10);
    }

    public isNewerThan(version: Version) {
        if (this.major !== version.major) {
            if (this.major > version.major) {
                return true;
            } else {
                return false;
            }
        }
        if (this.minor !== version.minor) {
            if (this.minor > version.minor) {
                return true;
            } else {
                return false;
            }
        }
        if (this.fix !== version.fix) {
            if (this.fix > version.fix) {
                return true;
            } else {
                return false;
            }
        }
    }

    toString(): string {
        return this.major + '.' + this.minor + '.' + this.fix;
    }
}
