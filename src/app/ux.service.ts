import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {OnlineService} from './online.service';

@Injectable({
    providedIn: 'root'
})
export class UXService {

    constructor(public toast: ToastController) {
    }

    showSuccess(text: string) {
        const toast = this.toast.create({
            message: text,
            duration: 5000
        });
        toast.then(value => {
            value.present();
        });
    }

    showError(error: string) {
        console.error(error);
        const toast = this.toast.create({
            message: error,
            duration: 5000
        });
        toast.then(value => {
            value.present();
        });
    }


    showNetworkError(error: any, message: string = null) {
        let e = error;
        if (e.hasOwnProperty('error') && e.error != null && e.hasOwnProperty('message')) {
            if (e.error.hasOwnProperty('message')) {
                e = e.error.message;
                // if server wants you to log in
                if (e === 'You need to be logged in') {
                    // TODO
                }
                if (message == null) {
                    message = 'Could not connect to server';
                }
            } else {
                e = e.message;
            }
        } else {
            e = JSON.stringify(error);
        }
        if (message == null) {
            message = 'Could not get data';
        }

        if (e.startsWith('Http failure')) {
            e = e.substring(e.lastIndexOf(':') + 1);
        }
        const m = message + ': ' + e;
        console.error(m);
        const toast = this.toast.create({
            message: m,
            duration: 5000
        });
        toast.then(value => {
            value.present();
        });
    }

}
