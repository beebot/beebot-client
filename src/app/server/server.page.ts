import {Component, OnInit} from '@angular/core';
import {DataState, OnlineService} from '../online.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-server',
    templateUrl: './server.page.html',
    styleUrls: ['./server.page.scss'],
})
export class ServerPage implements OnInit {

    state: DataState = DataState.LOADING;

    constructor(public online: OnlineService, public router: Router) {
        this.reload();
    }

    ngOnInit() {
    }

    reload() {
        this.online.serverUpdate();
    }


}
