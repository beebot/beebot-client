import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {ModuleCreationPage} from './module-creation.page';
import {ModuleComponentsModule} from '../module-components/module-components.module';

const routes: Routes = [
    {
        path: '',
        component: ModuleCreationPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ModuleComponentsModule
    ],
    declarations: [ModuleCreationPage]
})
export class ModuleCreationPageModule {
}
