import {Component, OnInit} from '@angular/core';
import {ModuleDataObject, ModuleInfo, OnlineService, PrivateChannelsData, Tuple} from '../online.service';
import {CurrencyService} from '../currency.service';
import {UXService} from '../ux.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-module-creation',
    templateUrl: './module-creation.page.html',
    styleUrls: ['./module-creation.page.scss'],
})
export class ModuleCreationPage implements OnInit {

    modules: Array<ModuleInfo> = [];

    modulesNonForced: Array<ModuleInfo> = [];

    selectedModule: ModuleInfo;

    data: ModuleDataObject;

    sid: string;


    constructor(public online: OnlineService, public currency: CurrencyService, public ux: UXService, public router: Router, public route: ActivatedRoute) {
        this.online.moduleAll().subscribe(value => {
            this.modules = value;
            this.modules.forEach(value1 => {
                if (!value1.forced)
                    this.modulesNonForced.push(value1);
            });
        });
    }

    public setModule(module: ModuleInfo) {
        this.data = this.currency.genModuleData(module.uniqueName);
        this.selectedModule = module;
    }


    send() {
        this.online.moduleCreate(this.online.selectedServer, this.selectedModule, this.data).subscribe(value => {
            this.ux.showSuccess('Module was created');
            this.router.navigate(['/modules/' + this.sid]);
        }, error1 => this.ux.showNetworkError(error1));
    }

    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');
    }

}
