import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokePage } from './poke.page';

describe('PokePage', () => {
  let component: PokePage;
  let fixture: ComponentFixture<PokePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
