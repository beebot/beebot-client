import {Component, OnInit} from '@angular/core';
import {BasicModuleReference, BasicServerStat, ModuleInfo, OnlineService} from '../online.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-poke',
    templateUrl: './poke.page.html',
    styleUrls: ['./poke.page.scss'],
})
export class PokePage implements OnInit {

    sid: string;

    constructor(public online: OnlineService, public router: Router, public ux: UXService, private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');
        this.reload();
    }

    reload() {

    }

}
