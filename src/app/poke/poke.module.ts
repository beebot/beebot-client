import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {PokePage} from './poke.page';
import {ModuleComponentsModule} from '../module-components/module-components.module';

const routes: Routes = [
    {
        path: '',
        component: PokePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ModuleComponentsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PokePage]
})
export class PokePageModule {
}
