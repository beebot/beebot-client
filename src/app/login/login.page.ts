import {Component, OnInit} from '@angular/core';
import {LoadingController, MenuController, ToastController} from '@ionic/angular';
import {OnlineService} from '../online.service';
import {UXService} from '../ux.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    mail: string;
    password: string;


    loggingIn = false;

    constructor(public menuCtrl: MenuController, public online: OnlineService, public loading: LoadingController, public ux: UXService, public router: Router) {
        // menuCtrl.enable(false);
    }

    ngOnInit() {
        if (this.online.token)
            this.router.navigateByUrl('/server');
    }

    ionViewWillEnter() {
    }

    async login() {
        if (this.loggingIn) {
            return;
        }
        this.loggingIn = true;
        this.online.login(this.mail, this.password).subscribe(value => {
            console.log('logged in with token ' + value.token);
            this.loggingIn = false;
            this.online.setToken(value.token);
            this.online.serverUpdate();
            this.ux.showSuccess('Welcome back, ' + value.name);
            this.router.navigateByUrl('/server');
        }, error1 => {
            this.loggingIn = false;
            console.error('Could not log in ' + JSON.stringify(error1.ti));
            this.ux.showNetworkError(error1, 'Could not log you in');
        });
    }

}
