import {Injectable} from '@angular/core';
import {
    AntiSpamData,
    ChannelReference,
    CtafkData,
    MessageData,
    ModuleDataObject,
    OnlineService,
    PrivateChannelsData,
    ServerGroupReference,
    SupportData
} from './online.service';

@Injectable({
    providedIn: 'root'
})
export class CurrencyService {

    replacer = [
        {'username': 'The users username'}
    ];

    constructor(online: OnlineService) {
    }

    genModuleData(uniqueName: string): ModuleDataObject {
        let data;
        switch (uniqueName) {
            case 'privatechannelz':
                data = new PrivateChannelsData();
                data.deleteDelay = 30;
                data.channelName = '%username%\'s channel';
                data.channelDescription = 'This is the channel of the fabulous %username%!';
                data.message = 'This is your private Channel. Good luck and have fun!';
                return data;
            case 'ctafk':
                data = new CtafkData();
                data.channelName = 'Capture the AFK: %username%';
                data.channelDescription = 'Just join this channel to receive your stats';
                data.line = '#%ctafk_place% %username% with %ctafk_time% (%ctafk_percent% %)';
                data.showAll = true;
                data.showLatest = true;
                data.winnerLatest = true;
                data.updateTime = 10;
                data.maxUser = 15;
                return data;
            case 'antispam':
                data = new AntiSpamData();
                data.blockDefaultUsername = true;
                data.blockCensoredUsernames = true;
                data.blockChannelHopping = true;
                data.hoppingTimes = 3;
                data.hoppingSeconds = 15;
                data.activatedLists = [];
                data.ownBlockwords = [];
                data.ownWhitelist = [];
                return data;
            case 'support':
                data = new SupportData();
                data.userMessage = 'Hello %username%, our support team is contacted. Please wait...';
                data.supportMessage = 'User %username% requested support';
                data.description = 'Join this channel to get support from our team';
                data.channel = new ChannelReference();
                data.group = new ServerGroupReference();
                data.showOnline = true;
                return data;
            case 'msgprovider':
                data = new MessageData();
                data.group = new ServerGroupReference();
                data.channel = new ChannelReference();
                data.start = null;
                data.end = null;
                data.message = 'Hello there!';
                return data;
        }
        if (data == null) {
            console.error('Tried to generate invalid data named ' + uniqueName);
        }
    }
}
