import {Component, Input, OnInit} from '@angular/core';
import {BasicModuleReference, BasicServerStat, DataState, ModuleInfo, OnlineService} from '../../online.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UXService} from '../../ux.service';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-modules',
    templateUrl: './modules.component.html',
    styleUrls: ['./modules.component.scss'],
})
export class ModulesComponent implements OnInit {


    @Input() sid: string;

    modules: Array<ModuleInfo> = [];
    modulesState: DataState = DataState.LOADING;

    activeModules: Array<BasicModuleReference> = [];

    server: BasicServerStat;

    constructor(public online: OnlineService, public router: Router, public ux: UXService, private route: ActivatedRoute, public alert: AlertController) {

    }

    ngOnInit() {
        this.reload();
    }

    editModule(module: BasicModuleReference) {
        this.router.navigate(['/modules/' + this.sid + '/edit/' + module.id]);
    }

    async deleteModule(module: BasicModuleReference) {
        const alert = this.alert.create({
            header: 'Delete Module?',
            subHeader: '',
            message: 'Do you want to delete the module?',
            buttons: [
                {
                    text: 'Yes',
                    handler: (x) => {
                        console.log('deleted module');
                        this.online.moduleDelete(this.sid, module.id).subscribe(value => {
                            this.ux.showSuccess('Module ' + module.name + ' has been deleted');
                            this.activeModules.splice(this.activeModules.indexOf(module), 1);
                            this.reload();
                        }, error1 => {
                            this.ux.showNetworkError(error1);
                        });
                    }
                }, {
                    text: 'No',
                    handler: (x) => {
                        console.log('not deleted');
                    }
                }
            ],
        });
        alert.then((a) => {
            a.present();
        });
    }

    reload() {
        this.modulesState = DataState.LOADING;
        this.online.moduleAll().subscribe(value => {
            this.modules = value;
        }, error1 => this.ux.showNetworkError(error1));
        this.online.moduleActive(this.sid).subscribe(value => {
            this.activeModules = value;
            this.modulesState = DataState.OKAY;
        }, error1 => {
            this.modulesState = DataState.WRONG;
            this.ux.showNetworkError(error1);
        });
    }


    openLog(module: BasicModuleReference) {
        this.router.navigate(['/modules/' + this.sid + '/log/' + module.id]);
    }
}
