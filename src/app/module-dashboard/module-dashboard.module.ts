import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {ModulesComponent} from './modules/modules.component';
import {OverviewComponent} from './overview/overview.component';
import {ModuleMonitorModule} from '../module-monitor/module-monitor.module';
import {InfoComponent} from './info/info.component';
import {GraphsComponent} from './graphs/graphs.component';
import {ClientsComponent} from './clients/clients.component';

@NgModule({
    declarations: [ModulesComponent, OverviewComponent, InfoComponent, GraphsComponent, ClientsComponent],
    exports: [ModulesComponent, OverviewComponent, InfoComponent, GraphsComponent, ClientsComponent],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ModuleMonitorModule
    ]
})
export class ModuleDashboardModule {
}
