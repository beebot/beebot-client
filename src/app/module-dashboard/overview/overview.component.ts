import {Component, Input, OnInit} from '@angular/core';
import {BasicServerStat, ServerTreeItem} from '../../online.service';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {

    @Input() stats: BasicServerStat;
    @Input() tree: ServerTreeItem;

    constructor() {
    }

    ngOnInit() {
    }

}
