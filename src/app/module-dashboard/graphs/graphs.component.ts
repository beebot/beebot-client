import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-graphs',
    templateUrl: './graphs.component.html',
    styleUrls: ['./graphs.component.scss'],
})
export class GraphsComponent implements OnInit {

    @Input() sid: string;
    @Input() graphMinutes: number;

    uiSizes = [
        {xl: 6},
        {xl: 6},
        {xl: 6},
        {xl: 6},
        {xl: 6},
    ];

    constructor() {
    }

    ngOnInit() {
        setInterval(() => {
            console.log(this.uiSizes[1].xl);
        }, 1000);
    }

    toogleFullView() {
        this.uiSizes.forEach((value, index) => {
            this.uiSizes[index].xl = value.xl === 12 ? 6 : 12;
        });
    }

}
