import {Component, Input, OnInit} from '@angular/core';
import {BasicServerStat} from '../../online.service';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {


    @Input() stat: BasicServerStat;

    constructor() {
    }

    ngOnInit() {
    }

}
