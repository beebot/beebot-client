import {Component, Input, OnInit} from '@angular/core';
import {DetailedClientView, OnlineService} from '../../online.service';
import {UXService} from '../../ux.service';

@Component({
    selector: 'app-clients',
    templateUrl: './clients.component.html',
    styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit {

    @Input() sid: string;


    clients: Array<DetailedClientView>;

    constructor(public online: OnlineService, public ux: UXService) {
    }

    ngOnInit() {
        this.update();
    }

    update() {
        this.online.teamspeakClients(this.sid).subscribe(value => {
            this.clients = value;
        }, error => this.ux.showNetworkError(error));
    }

}
