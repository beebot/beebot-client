import { ModuleComponentsModule } from './module-components.module';

describe('ModuleComponentsModule', () => {
  let moduleComponentsModule: ModuleComponentsModule;

  beforeEach(() => {
    moduleComponentsModule = new ModuleComponentsModule();
  });

  it('should create an instance', () => {
    expect(moduleComponentsModule).toBeTruthy();
  });
});
