import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtafkDataComponent } from './ctafk-data.component';

describe('CtafkDataComponent', () => {
  let component: CtafkDataComponent;
  let fixture: ComponentFixture<CtafkDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtafkDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtafkDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
