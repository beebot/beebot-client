import {Component, Input, OnInit} from '@angular/core';
import {CtafkData, PrivateChannelsData} from '../../online.service';

@Component({
    selector: 'app-ctafk-data',
    templateUrl: './ctafk-data.component.html',
    styleUrls: ['./ctafk-data.component.scss']
})
export class CtafkDataComponent implements OnInit {

    @Input() sid: string;
    @Input() data: CtafkData;

    advanced: boolean;

    constructor() {
    }

    ngOnInit() {
    }

}
