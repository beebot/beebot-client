import {Component, Input, OnInit} from '@angular/core';
import {ChannelReference, OnlineService, Tuple} from '../../online.service';

@Component({
    selector: 'app-channel-input',
    templateUrl: './channel-input.component.html',
    styleUrls: ['./channel-input.component.scss'],
})
export class ChannelInputComponent implements OnInit {

    @Input() sid: string;
    @Input() channelReference: ChannelReference;

    @Input() text: string;

    channels: Array<Tuple>;

    constructor(public online: OnlineService) {
    }

    ngOnInit() {
        this.reloadChannel();
    }

    reloadChannel() {
        this.online.teamspeakChannel(this.sid).subscribe(value => {
            this.channels = value;
        }, error1 => {
            console.log(error1);
        });
    }

}
