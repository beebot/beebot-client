import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelgroupInputComponent } from './channelgroup-input.component';

describe('ChannelgroupInputComponent', () => {
  let component: ChannelgroupInputComponent;
  let fixture: ComponentFixture<ChannelgroupInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelgroupInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelgroupInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
