import {Component, Input, OnInit} from '@angular/core';
import {ChannelGroupReference, OnlineService, Tuple} from '../../online.service';

@Component({
    selector: 'app-channelgroup-input',
    templateUrl: './channelgroup-input.component.html',
    styleUrls: ['./channelgroup-input.component.scss']
})
export class ChannelgroupInputComponent implements OnInit {

    @Input() sid: string;
    @Input() text: string;
    @Input() channelgroupReference: ChannelGroupReference;

    channelGroups: Array<Tuple>;

    constructor(public online: OnlineService) {
    }

    ngOnInit() {
        this.reloadChannelGroups();
    }

    reloadChannelGroups() {
        this.online.teamspeakChannelGroups(this.sid).subscribe(value => {
            this.channelGroups = value;
        }, error1 => {
            console.log(error1);
        });
    }

}
