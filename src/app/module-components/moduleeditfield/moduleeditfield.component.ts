import {Component, Input, OnInit} from '@angular/core';
import {ModuleDataObject} from '../../online.service';

@Component({
    selector: 'app-moduleeditfield',
    templateUrl: './moduleeditfield.component.html',
    styleUrls: ['./moduleeditfield.component.scss']
})
export class ModuleeditfieldComponent implements OnInit {

    @Input() sid: string;
    @Input() id: number;
    @Input() moduleUniqueName: string;
    @Input() data: ModuleDataObject;

    constructor() {
    }

    ngOnInit() {
    }


}
