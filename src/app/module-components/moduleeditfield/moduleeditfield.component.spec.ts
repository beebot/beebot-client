import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleeditfieldComponent } from './moduleeditfield.component';

describe('ModuleeditfieldComponent', () => {
  let component: ModuleeditfieldComponent;
  let fixture: ComponentFixture<ModuleeditfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleeditfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleeditfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
