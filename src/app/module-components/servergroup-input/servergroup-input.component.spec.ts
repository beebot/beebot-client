import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServergroupInputComponent } from './servergroup-input.component';

describe('ServergroupInputComponent', () => {
  let component: ServergroupInputComponent;
  let fixture: ComponentFixture<ServergroupInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServergroupInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServergroupInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
