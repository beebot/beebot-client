import {Component, Input, OnInit} from '@angular/core';
import {ChannelGroupReference, OnlineService, ServerGroupReference, Tuple} from '../../online.service';

@Component({
    selector: 'app-servergroup-input',
    templateUrl: './servergroup-input.component.html',
    styleUrls: ['./servergroup-input.component.scss']
})
export class ServergroupInputComponent implements OnInit {

    @Input() sid: string;
    @Input() text: string;
    @Input() servergroupReference: ServerGroupReference;

    serverGroups: Array<Tuple>;

    constructor(public online: OnlineService) {
    }

    ngOnInit() {
        this.reloadChannelGroups();
    }

    reloadChannelGroups() {
        this.online.teamspeakServerGroups(this.sid).subscribe(value => {
            this.serverGroups = value;
        }, error1 => {
            console.log(error1);
        });
    }
}
