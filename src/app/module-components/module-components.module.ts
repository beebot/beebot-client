import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChannelInputComponent} from './channel-input/channel-input.component';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {PrivatechannelzDataComponent} from './privatechannelz-data/privatechannelz-data.component';
import {ChannelgroupInputComponent} from './channelgroup-input/channelgroup-input.component';
import {ModuleinfoComponent} from './moduleinfo/moduleinfo.component';
import {ModuleeditfieldComponent} from './moduleeditfield/moduleeditfield.component';
import {CtafkDataComponent} from './ctafk-data/ctafk-data.component';
import {AntispamDataComponent} from './antispam-data/antispam-data.component';
import {SupportDataComponent} from './support-data/support-data.component';
import {ServergroupInputComponent} from './servergroup-input/servergroup-input.component';
import {UserPokeComponent} from './user-poke/user-poke.component';
import { MessageDataComponent } from './message-data/message-data.component';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule
    ],
    entryComponents: [UserPokeComponent],
    declarations: [ChannelInputComponent, PrivatechannelzDataComponent, ChannelgroupInputComponent, ModuleinfoComponent, ModuleeditfieldComponent, CtafkDataComponent, AntispamDataComponent, SupportDataComponent, ServergroupInputComponent, UserPokeComponent, MessageDataComponent],
    exports: [ChannelInputComponent, PrivatechannelzDataComponent, ChannelgroupInputComponent, ModuleinfoComponent, ModuleeditfieldComponent, CtafkDataComponent, AntispamDataComponent, SupportDataComponent, ServergroupInputComponent, UserPokeComponent]
})
export class ModuleComponentsModule {
}
