import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivatechannelzDataComponent } from './privatechannelz-data.component';

describe('PrivatechannelzDataComponent', () => {
  let component: PrivatechannelzDataComponent;
  let fixture: ComponentFixture<PrivatechannelzDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivatechannelzDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivatechannelzDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
