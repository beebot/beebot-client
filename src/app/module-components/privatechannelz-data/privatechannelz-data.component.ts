import {Component, Input, OnInit} from '@angular/core';
import {OnlineService, PrivateChannelsData} from '../../online.service';

@Component({
    selector: 'app-privatechannelz-data',
    templateUrl: './privatechannelz-data.component.html',
    styleUrls: ['./privatechannelz-data.component.scss']
})
export class PrivatechannelzDataComponent implements OnInit {

    @Input() sid: string;
    @Input() data: PrivateChannelsData;

    constructor(public online: OnlineService) {
    }

    ngOnInit() {
    }

}
