import {Component, Input, OnInit} from '@angular/core';
import {BasicModuleReference} from '../../online.service';

@Component({
    selector: 'app-moduleinfo',
    templateUrl: './moduleinfo.component.html',
    styleUrls: ['./moduleinfo.component.scss']
})
export class ModuleinfoComponent implements OnInit {

    @Input() module: BasicModuleReference;

    constructor() {
    }

    ngOnInit() {
    }

}
