import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleinfoComponent } from './moduleinfo.component';

describe('ModuleinfoComponent', () => {
  let component: ModuleinfoComponent;
  let fixture: ComponentFixture<ModuleinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
