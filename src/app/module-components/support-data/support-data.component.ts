import {Component, Input, OnInit} from '@angular/core';
import {SupportData} from '../../online.service';

@Component({
  selector: 'app-support-data',
  templateUrl: './support-data.component.html',
  styleUrls: ['./support-data.component.scss']
})
export class SupportDataComponent implements OnInit {

  @Input() sid: string;
  @Input() data: SupportData;


  constructor() { }

  ngOnInit() {
  }

}
