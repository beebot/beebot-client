import {Component, Input, OnInit} from '@angular/core';
import {MessageData} from '../../online.service';

@Component({
    selector: 'app-message-data',
    templateUrl: './message-data.component.html',
    styleUrls: ['./message-data.component.scss']
})
export class MessageDataComponent implements OnInit {

    @Input() data: MessageData;
    @Input() sid: string;

    checkChannel = 'false';
    checkGroup = 'false';
    checkTime = 'false';

    constructor() {
    }

    ngOnInit() {
        if (this.data.channel.channelId != -1) {
            this.checkChannel = 'true';
        }
        if (this.data.group.groupId != -1) {
            this.checkGroup = 'true';
        }
        if (this.data.end != null || this.data.start != null) {
            this.checkTime = 'true';
        }
        this.updateData();
    }

    initTimes() {
        this.data.start = new Date().toISOString();

        const d2 = new Date();
        d2.setSeconds(0);
        d2.setMilliseconds(0);
        d2.setMinutes(0);
        d2.setHours(0);
        d2.setDate(d2.getDate() + 3);
        this.data.end = d2.toISOString();
    }

    updateTime() {
        this.initTimes();
        this.updateData();
    }

    updateData() {
        if (this.checkChannel == 'false') {
            this.data.channel.channelId = -1;
        } else {
            if (this.data.channel.channelId == -1) {
                this.data.channel.channelId = 1;
            }
        }

        if (this.checkGroup == 'false') {
            this.data.group.groupId = -1;
        } else {
            if (this.data.group.groupId == -1) {
                this.data.group.groupId = 1;
            }
        }

        if (this.checkTime == 'false') {
            this.data.start = null;
            this.data.end = null;
        }

        console.log(JSON.stringify(this.data));
    }

}
