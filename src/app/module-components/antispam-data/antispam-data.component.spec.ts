import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AntispamDataComponent } from './antispam-data.component';

describe('AntispamDataComponent', () => {
  let component: AntispamDataComponent;
  let fixture: ComponentFixture<AntispamDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AntispamDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AntispamDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
