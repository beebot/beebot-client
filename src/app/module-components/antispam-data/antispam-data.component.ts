import {Component, Input, OnInit} from '@angular/core';
import {AntiSpamData, BlockedWordList, OnlineService} from '../../online.service';
import {UXService} from '../../ux.service';
import Block = jasmine.Block;

@Component({
    selector: 'app-antispam-data',
    templateUrl: './antispam-data.component.html',
    styleUrls: ['./antispam-data.component.scss']
})
export class AntispamDataComponent implements OnInit {

    @Input() data: AntiSpamData;
    @Input() sid: string;

    showExtended = false;

    blockedWords: Array<BlockedWordList>;

    usedWords: Array<BlockedWordList>;
    showUsedWords = true;

    currentBlockWord;

    currentWhitelistWord;

    strategies = [
        {
            id: 'JUST_INFORM',
            title: 'Poke user',
            desc: 'Just pokes the user, informing him'
        }, {
            id: 'KICK',
            title: 'Kick user',
            desc: 'Kicks the user immediatly from the server'
        }, {
            id: 'KICK_BAN_LIKE_CSGO',
            title: 'Kick, then ban like CSGO',
            desc: 'Kicks the user. Then bans 30 minutes, 2 hours and 1 week (repeat).'
        }, {
            id: 'KICK_BAN_LIKE_CSGO_PERMA',
            title: 'Kick, then ban like CSGO and permanently',
            desc: 'Kicks the user. Then bans 30 minutes, 2 hours, 1 week and permanently'
        }, {
            id: 'KICK_PERMA',
            title: 'Kicks & Perma-Ban',
            desc: 'Kick and then perma bans. It\'s that easy'
        }
    ];

    stratDesc = '';

    constructor(public online: OnlineService, public ux: UXService) {
    }

    ngOnInit() {
        this.online.miscBlockedWords().subscribe(value => {
            this.blockedWords = value;
        }, error1 => this.ux.showNetworkError(error1));
    }

    strat() {
        this.stratDesc = this.strategies.find(x => x.id === this.data.strategy).desc;
    }

    changeShowUsedWords() {
        this.showUsedWords = !this.showUsedWords;
    }

    updateWords() {
        this.usedWords = [];
        this.data.activatedLists.forEach(data => {
            this.blockedWords.filter(x => x.name === data).forEach(x => {
                this.usedWords.push(x);
            });
        });
    }

    addCurrentBlockWord() {
        if (this.currentBlockWord.length < 4) {
            this.ux.showError('For your own security: You cannot define a word shorter than 4 characters');
            return;
        }
        this.data.ownBlockwords.push(this.currentBlockWord);
        this.data.ownBlockwords.sort();
        this.ux.showSuccess('Added blacklist word \'' + this.currentBlockWord + '\'');
        this.currentBlockWord = '';
    }

    removeOwnWord(word: string) {
        const index = this.data.ownBlockwords.indexOf(word, 0);
        if (index > -1) {
            this.data.ownBlockwords.splice(index, 1);
            this.ux.showSuccess('Removed blacklist word \'' + word + '\'');
        }
    }

    removeOwnWhitelist(word: string) {
        const index = this.data.ownWhitelist.indexOf(word, 0);
        if (index > -1) {
            this.data.ownWhitelist.splice(index, 1);
            this.ux.showSuccess('Removed whitelist word \'' + word + '\'');
        }
    }

    addCurrentWhitelistWord() {
        if (this.currentWhitelistWord.length < 4) {
            this.ux.showError('For your own security: You cannot define a word shorter than 4 characters');
            return;
        }
        this.data.ownWhitelist.push(this.currentWhitelistWord);
        this.data.ownWhitelist.sort();
        this.ux.showSuccess('Added whitelist word \'' + this.currentBlockWord + '\'');
        this.currentWhitelistWord = '';
    }

}
