import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPokeComponent } from './user-poke.component';

describe('UserPokeComponent', () => {
  let component: UserPokeComponent;
  let fixture: ComponentFixture<UserPokeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPokeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
