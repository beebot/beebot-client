import {Component, Input, OnInit} from '@angular/core';
import {OnlineService, PokeBody} from '../../online.service';
import {UXService} from '../../ux.service';

@Component({
    selector: 'app-user-poke',
    templateUrl: './user-poke.component.html',
    styleUrls: ['./user-poke.component.scss']
})
export class UserPokeComponent implements OnInit {

    @Input() sid: string;

    users: Array<PokeSelected>;

    message: string;

    selectedTemplate: string;

    templates = [
        {
            title: 'Server shutdown',
            message: 'Attention! The server is going to shut down in a few minutes'
        }, {
            title: 'Event',
            message: 'Hello %username%, join this event on our server'
        }
    ];

    constructor(public online: OnlineService, public ux: UXService) {
    }

    reload() {
        this.users = null;
        this.online.teamspeakUser(this.sid).subscribe(value => {
            this.users = [];
            value.forEach(value1 => {
                const p = new PokeSelected();
                p.id = value1.key;
                p.name = value1.value;
                this.users.push(p);
            });
        }, error1 => this.ux.showNetworkError(error1));
    }

    templateChange() {
        this.message = this.selectedTemplate;
    }

    selectAll() {
        this.users.forEach(value => {
            value.selected = true;
        });
    }

    selectNone() {
        this.users.forEach(value => {
            value.selected = false;
        });
    }

    send() {
        if (!this.message || this.message.length < 5) {
            this.ux.showError('Your message must be at least 5 characters long');
            return;
        }
        const ids: Array<number> = [];
        this.users.forEach(value => {
            if (value.selected) {
                console.log('user ' + value.name + ' selected');
                ids.push(value.id);
            }
        });
        if (ids.length === 0) {
            this.ux.showError('You have to select an user...');
            return;
        }

        const poke = new PokeBody();
        poke.ids = ids;
        poke.message = this.message;

        this.online.teamspeakPoke(this.sid, poke).subscribe(value => {
            let concat = '';
            value.forEach(err => {
                concat += err + '\n';
            });
            if (concat !== '') {
                this.ux.showError(concat);
            } else {
                this.ux.showSuccess('Poked all user');
            }
        }, error1 => this.ux.showNetworkError(error1));
    }

    ngOnInit() {
        this.reload();
    }


}

class PokeSelected {
    name: string;
    id: number;
    selected: boolean;
}
