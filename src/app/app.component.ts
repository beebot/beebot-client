import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {OnlineService} from './online.service';
import {UXService} from './ux.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home',
            showLogin: true,
            showLogoff: true,
            advRouting: false
        },
        {
            title: 'Log in',
            url: '/login',
            icon: 'log-in',
            showLogin: false,
            showLogoff: true,
            advRouting: false
        }, {
            title: 'Server List',
            url: '/server',
            icon: 'switch',
            showLogin: true,
            showLogoff: false,
            advRouting: false

        }
    ];


    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public online: OnlineService,
        public ux: UXService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
        setTimeout(() => {
            this.online.userStatus().subscribe(value => {
                console.log('privilege is ' + value.privilege);
                if (value.privilege.toString() === 'ADMIN') {
                    console.log('creating admin pages...');
                    const page = {
                        title: '[Admin] Page',
                        url: '/log',
                        icon: 'document',
                        showLogin: true,
                        showLogoff: false,
                        advRouting: false
                    };
                    this.appPages.push(page);
                }
            }, error1 => this.ux.showNetworkError(error1));
        }, 3000);


        /*this.online.userStatus().subscribe(value => {
             if (value.privilege === Privileges.ADMIN) {
                 const page = {
                     title: '[Admin] Logs',
                     url: '/log',
                     icon: '',
                     showLogin: true,
                     showLogoff: false,
                     advRouting: false
                 };
                 this.appPages.push(page);
             }
         }, error1 => this.ux.showNetworkError(error1));*/
    }
}
