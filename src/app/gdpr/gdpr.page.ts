import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OnlineService} from '../online.service';

@Component({
  selector: 'app-gdpr',
  templateUrl: './gdpr.page.html',
  styleUrls: ['./gdpr.page.scss'],
})
export class GdprPage implements OnInit {

  key: string;
  status;
  gdprData;

  expand = false;
  done = false;

  constructor(public route: ActivatedRoute, public online: OnlineService) {
  }

  download() {
    const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.gdprData, null, 2));
    const dlAnchorElem = document.getElementById('downloadAnchorElem');
    dlAnchorElem.setAttribute('href', dataStr);
    dlAnchorElem.setAttribute('download', 'scene.json');
    dlAnchorElem.click();
  }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('key');
      if (this.key === 'show') {
      this.key = null;
    }
    if (this.key != null) {
      this.status = '(Loading)';
      this.online.gdprGet(this.key).subscribe(value => {
        this.done = true;
        this.gdprData = value;
      }, error1 => {
        this.status = 'Error fetching GDPR data' + error1;
      });
    }
  }

}
