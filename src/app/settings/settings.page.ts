import {Component, OnInit} from '@angular/core';
import {OnlineService, PasswordChangeData, SessionData} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    data: SessionData = new SessionData();


    oldPassword: string;
    newPassword: string;
    newPassword2: string;

    constructor(public online: OnlineService, public ux: UXService) {
        this.data.name = 'User';
        this.data.mail = '';
    }

    ngOnInit() {
        this.online.userStatus().subscribe(value => {
            this.data = value;
        }, error1 => this.ux.showNetworkError(error1));
    }

    changePassword() {
        if (this.oldPassword.length < 6 || this.newPassword.length < 6) {
            this.ux.showError('Your password must at least contain six characters');
            return;
        }
        if (this.newPassword !== this.newPassword2) {
            this.ux.showError('Your new password doesn\'t match');
            return;
        }
        const data: PasswordChangeData = {
                oldPassword: this.oldPassword,
                newPassword: this.newPassword2
            }
        ;
        this.online.userPasswordChange(data).subscribe(value => {
            this.ux.showSuccess('Your password has been changed');
        }, error1 => this.ux.showNetworkError(error1, 'Your password couldn\'t been changed'));
    }

}
