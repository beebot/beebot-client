import {Component} from '@angular/core';
import {OnlineService} from '../online.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    modules = [
        {
            icon: 'briefcase',
            title: 'For everyone',
            description: 'From a personal server to an enterprise server. From one to thousands of users.'
        },
        {
            icon: 'remove-circle-outline',
            title: 'Anti-Spam',
            description: 'Keep away spammer and block specific usernames you don\'t want to have on your server'
        }, {
            icon: 'medkit',
            title: 'Stay secure',
            description: 'Check your server health. Get information for new server updates'
        },
        {
            icon: 'contacts',
            title: 'Support your user',
            description: 'Create an unique support experience for your user. Get notified when a user needs help.'
            /* }, {
                 icon: 'cafe',
                 title: 'Capture The AFK',
                 description: 'Create your own CaptureTheAFK channel (no copyright). Get a list on who is online for the most time.'
             */
        }, {
            icon: 'pricetag',
            title: 'Price? Free',
            description: 'Register now for free. You don\'t need to pay anything if you don\'t want to.'
        }
    ];

    constructor(public online: OnlineService) {

    }

}
