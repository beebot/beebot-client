import {inject, TestBed} from '@angular/core/testing';

import {UXService} from './ux.service';

describe('UXService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UXService]
    });
  });

  it('should be created', inject([UXService], (service: UXService) => {
    expect(service).toBeTruthy();
  }));
});
