import {Component, OnInit} from '@angular/core';
import {OnlineService, UserBeanState} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-log',
    templateUrl: './log.page.html',
    styleUrls: ['./log.page.scss'],
})
export class LogPage implements OnInit {

    logs: Array<string> = [];
    selectedLog: string;

    logLines: Array<string>;
    logLinesFiltered: Array<string>;

    filter = '';


    user: Array<Array<UserBeanState>>;

    loadingLog = false;

    constructor(public online: OnlineService, public ux: UXService) {
    }

    ngOnInit() {
        this.online.adminLogs().subscribe(value => {
            this.logs = value;
            if (value.length > 0) {
                this.selectedLog = value[0];
                this.selectLog();
            }
        }, error1 => {
            this.ux.showNetworkError(error1);
        });
        this.online.adminUser().subscribe(value => {
            // @ts-ignore
            this.user = value;
        }, error1 => {
            this.ux.showNetworkError(error1);
        });
    }

    selectLog() {
        this.logLines = null;
        this.logLinesFiltered = null;
        this.loadingLog = true;
        console.log('showing log ' + this.selectedLog);
        this.online.adminLog(this.selectedLog).subscribe(value => {
            this.logLines = value;
            this.logLinesFiltered = value;
            this.loadingLog = false;
            this.filter = '';
        }, error1 => {
            this.ux.showNetworkError(error1);
            this.loadingLog = false;
        });
    }

    filterLogs() {
        console.log('Filtering logs \'' + this.filter + '\'');
        this.logLinesFiltered = [];
        this.logLines.forEach(value => {
            if (value.indexOf(this.filter) !== -1) {
                this.logLinesFiltered.push(value);
            }
        });
    }

}
