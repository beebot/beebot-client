import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BasicModuleReference, ModuleDataObject, OnlineService} from '../online.service';
import {UXService} from '../ux.service';

@Component({
    selector: 'app-module-edit',
    templateUrl: './module-edit.page.html',
    styleUrls: ['./module-edit.page.scss'],
})
export class ModuleEditPage implements OnInit {

    sid: string;
    id: number;

    module: BasicModuleReference;

    data: ModuleDataObject;

    constructor(public online: OnlineService, public ux: UXService, public route: ActivatedRoute, public router: Router) {
    }

    ngOnInit() {
        this.sid = this.route.snapshot.paramMap.get('sid');
        this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.online.moduleInfo(this.sid, this.id).subscribe(value => {
            this.module = value;
        }, error1 => this.ux.showNetworkError(error1));
        this.online.moduleData(this.sid, this.id).subscribe(value => {
            this.data = value;
        }, error1 => this.ux.showNetworkError(error1));
    }

    send() {
        this.online.moduleChange(this.sid, this.id, this.data).subscribe(value => {
            this.ux.showSuccess('Module was edited');
            this.router.navigate(['/modules/' + this.sid]);
        }, error1 => this.ux.showNetworkError(error1));
        /*this.online.moduleCreate(this.online.selectedServer, this.selectedModule, this.data).subscribe(value => {
            this.ux.showSuccess('Module was created');
            this.router.navigate(['/modules/' + this.sid]);
        }, error1 => this.ux.showNetworkError(error1));*/
    }

}
