import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleEditPage } from './module-edit.page';

describe('ModuleEditPage', () => {
  let component: ModuleEditPage;
  let fixture: ComponentFixture<ModuleEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
