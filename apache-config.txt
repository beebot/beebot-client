# Config for secure Apache Angular Webserver settings

RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# Exclude Javascript, CSS and images
# Everything else will be routed to the index page
RewriteCond %{REQUEST_URI} !\.(?:css|js|map|jpe?g|gif|png|svg)$ [NC]
RewriteRule ^(.*)$ /index.html?path=$1 [NC,L,QSA]
